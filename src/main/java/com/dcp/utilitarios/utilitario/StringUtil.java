package com.dcp.utilitarios.utilitario;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

public class StringUtil {

    private static final Logger log = LoggerFactory.getLogger(StringUtil.class);
    private static ObjectMapper mapper = new ObjectMapper();




    /**
     * Método que convierte un jsonString a una lista
     * //@JsonInclude(JsonInclude.Include.NON_NULL) Agregar esta anotacion en la calse para que solo mapee los valores no nulos
     */
    public static <T> List<T> jsonToList(String jsonString, Class<T> clazz) {
        List<T> result = null;
        try {
            mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
            JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, clazz);
            result = mapper.readValue(jsonString, type);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return result;
    }

    /**
     * Otro método que convierte un jsonString a una lista
     * //@JsonInclude(JsonInclude.Include.NON_NULL) Agregar esta anotacion en la calse para que solo mapee los valores no nulos
     */
    public static <T> List<T> jsonToList2(String jsonStr, Class<T> clazz) {
        List<T> result = null;
        try {
            mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
            CollectionType typeFactory = mapper.getTypeFactory().constructCollectionType(List.class, clazz);
            result = mapper.readValue(jsonStr, typeFactory);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return result;
    }

    /**
     * Metodo que convierte un jsonString en un objeto
     *
     * //@JsonInclude(JsonInclude.Include.NON_NULL) Agregar esta anotacion en la calse para que solo mapee los valores no nulos
     *  mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
     */
    public static <T> T jsonToObject(String jsonStr, Class<T> clazz) {
        T object = null;
        try {
//            mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
            object = mapper.readValue(jsonStr, clazz);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return object;
    }
}
