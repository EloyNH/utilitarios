package com.dcp.utilitarios.utilitario;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUril {

    private static final Logger log = LoggerFactory.getLogger(DateUril.class);

    public static String dateToString(Date fechaDate, String formatString) {
        SimpleDateFormat format = new SimpleDateFormat(formatString);
        try {
            return format.format(fechaDate);
        } catch (Exception e) {
            log.error("Problema en el metodo 'dateToString'." + e.getMessage(), e);
            return null;
        }
    }
    public static Date stringToDate(String fechaString, String formatString) {
        SimpleDateFormat format = new SimpleDateFormat(formatString);
        try {
            return format.parse(fechaString);
        } catch (Exception e) {
            log.error("Problema en el metodo 'stringToDate'." + e.getMessage(), e);
            return null;
        }
    }
}
