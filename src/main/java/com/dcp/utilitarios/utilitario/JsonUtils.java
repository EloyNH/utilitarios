package com.dcp.utilitarios.utilitario;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import javax.annotation.PostConstruct;
import java.io.IOException;

public class JsonUtils {
    private ObjectMapper mapper;

    @PostConstruct
    public void init() {
        mapper = new ObjectMapper();
    }

    public <T> T parsearJson(String jsonClase, Class<T> nombreClase) throws IOException {
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);
        ObjectNode json = mapper.readValue(jsonClase, ObjectNode.class);
        return mapper.convertValue(json, nombreClase);
    }

    public <T> T parsearJsonUnknowProperties(String jsonClase, Class<T> nombreClase) throws IOException {
        mapper.setSerializationInclusion(JsonInclude.Include.ALWAYS);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        ObjectNode json = mapper.readValue(jsonClase, ObjectNode.class);
        return mapper.convertValue(json, nombreClase);
    }

    public String getStringJsonConNulls(Object object) throws JsonProcessingException {
        mapper.setSerializationInclusion(JsonInclude.Include.ALWAYS);
        return mapper.writeValueAsString(object);
    }

    public String getStringJson(Object object) throws JsonProcessingException {
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsString(object);
    }
}
